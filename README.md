# Supplemental data from #

Francis, WR., ML. Powers, SHD. Haddock (2014) [Characterization of an anthraquinone fluor from the bioluminescent, pelagic polychaete Tomopteris](http://onlinelibrary.wiley.com/doi/10.1002/bio.2671/full). Luminescence, 29, 1135-1140, doi: [10.1002/bio.2671](http://dx.doi.org/10.1002/bio.2671)

![francis2014_figure_1_new.jpg](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/francis2014_figure_1_new.jpg)

## Files ##
### figures ###
Individual PDFs of most figures and supplemental figures

* fig2_spectra.pdf
* fig3_chromatogram.pdf
* sfig_01_fluor_spectra.pdf
* sfig_02_meoac_extract.pdf
* sfig_03_lcms_trace.pdf
* sfig_04_hr_lcms_chromatogram.pdf
* sfig_05_hr_mass_spectrum.pdf
* sfig_06_hr_formula_model.pdf
* sfig_07_hr_lcms_aloe_emodin.pdf
* sfig_08_hr_lcms_sample_vs_aloe_emodin.pdf
* sfig_09_hr_lcmsms.pdf

### hplc ###
Shimdazu Nexera chromatogram from photo-diode array and fluorescence channel (ex. 450nm, em. 548nm)

Original figures that were not included in the paper:

* tcrude_031511_7_3d_chromatograms.pdf - chromatograms
* tcrude_031511_7_3d_extracted_RTs.pdf - PDA spectra at given retention times
* tcrude_031511_7_chromatogram_w_fl_2.pdf - chromatogram with fluorescence detector

### mass-spec ###
LTQ LCMS chromatogram and mass spectra

Thermo RAW files (gzipped) are in the [downloads tab](https://bitbucket.org/wrf/tomopteris-fluor-2014/downloads/)

### rscripts ###
R scripts used to generate figures, and other scripts to process data from a Shimadzu Nextera HPLC

* Hplc3DdataChromatogram.R - extract chromatograms of given wavelengths
* Hplc3DdataHeatmap.R - generate a heatmap of the 3D data
* Hplc3DdataSpectra.R - extract spectra from given retention times
* Hplc3DFluorChromatogram.R - plot chromatogram with fluorescence detector
* tomo-fluor_fig3_chromatogram.R
* tomo-fluor_fig4_aloe_emodin_thesis.R
* tomo-fluor_sfig4_ms_chromatogram.R
* wave2rgb.R - function to convert wavelength to approximate RGB values ([original repo here](https://bitbucket.org/wrf/wave2rgb/src/master/))

### uv-vis-spectra ###
UV-vis and some fluorescence spectra of various compounds used to confirm the identity of aloe-emodin

